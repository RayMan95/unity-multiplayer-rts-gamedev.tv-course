using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class JoinLobbyMenu : MonoBehaviour
{
    [SerializeField] private GameObject landingPagePanel = null;
    [SerializeField] private TMP_InputField addressInput = null;
    [SerializeField] private Button joinButton = null;
    [SerializeField] private TMP_Text errorMessageText = null;
    [SerializeField] private GameObject errorMessagePanel = null;

    private void OnEnable()
    {
        RTSNetworkManager.ClientOnConnected += HandleClientConnected;
        RTSNetworkManager.ClientOnDisconnected += HandleClientDisconnected;
    }

    private void OnDisable()
    {
        RTSNetworkManager.ClientOnConnected -= HandleClientConnected;
        RTSNetworkManager.ClientOnDisconnected -= HandleClientDisconnected;
    }

    public void Join()
    {
        string address = addressInput.text;

        if (IsValidNetworkAddress(address))
        {
            NetworkManager.singleton.networkAddress = address;
            NetworkManager.singleton.StartClient();

            joinButton.interactable = false;
            errorMessageText.text = "";
            errorMessagePanel.SetActive(false);
        }

        else
        {
            StartCoroutine(nameof(ErrorMessageWaitForSeconds));
        }
        
    }

    private void HandleClientConnected()
    {
        joinButton.interactable = true;

        gameObject.SetActive(false);
        landingPagePanel.SetActive(false);
    }

    private void HandleClientDisconnected()
    {
        joinButton.interactable = true;
    }

    IEnumerator ErrorMessageWaitForSeconds()
    {
        errorMessageText.text = "Error: Invalid address";
        errorMessagePanel.SetActive(true);
        
        yield return new WaitForSeconds(2f);
        
        errorMessageText.text = "";
        errorMessagePanel.SetActive(false);
    }

    private bool IsValidNetworkAddress(string address)
    {
        Regex validateIPv4Regex = new Regex("^(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

        List<string> allowedStrings = new List<string>(new string[]
        {
            "localhost",
            "your.mom's.house"
        });

        //if (validateIPv4Regex.IsMatch(address)) return true;
        if (allowedStrings.Contains(address)) return true;  

        return false;

    }
}
