using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Unit : NetworkBehaviour
{
    [SerializeField] private int cost = 10;
    [SerializeField] private Health health = null;
    [SerializeField] private UnityEvent onSelected;
    [SerializeField] private UnityEvent onDeselected;

    [SerializeField] private UnitMovement unitMovement;
    [SerializeField] private Targeter targeter;

    [SerializeField] private SpriteRenderer selectedSpriteRender;


    public static event Action<Unit> ServerOnUnitSpawned;
    public static event Action<Unit> ServerOnUnitDespawned;

    public static event Action<Unit> AuthorityOnUnitSpawned;
    public static event Action<Unit> AuthorityOnUnitDespawned;

    public UnitMovement GetUnitMovement()
    {
        return unitMovement;
    }

    public Targeter GetTargeter()
    {
        return targeter;
    }

    public int GetResourceCost()
    {
        return cost;
    }

    #region Server

    public override void OnStartServer()
    {
        health.ServerOnDie += ServerHandleDie;
        UnitBase.ServerOnPlayerDie += ServerHandlePlayerDie;

        ServerOnUnitSpawned?.Invoke(this);
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
        UnitBase.ServerOnPlayerDie -= ServerHandlePlayerDie;

        ServerOnUnitDespawned?.Invoke(this);
    }

    private void ServerHandleDie()
    {
        NetworkServer.Destroy(gameObject);
    }

    private void ServerHandlePlayerDie(int deadConnectionId)
    {
        if (deadConnectionId != this.connectionToClient.connectionId) return;

        
    }


    #endregion

    #region Client

    public override void OnStartAuthority()
    {
        AuthorityOnUnitSpawned?.Invoke(this);
    }

    public override void OnStopClient()
    {
        if (!isOwned) return;

        AuthorityOnUnitDespawned?.Invoke(this);
    }

    [Client]
    public void Select()
    {
        if (!isOwned) return;

        onSelected?.Invoke();
    }

    [Client]
    public void Deselect()
    {
        if (!isOwned) return;

        onDeselected?.Invoke();
    }


    #endregion
}
