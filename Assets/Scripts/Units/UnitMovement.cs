using Mirror;
using UnityEngine;
using UnityEngine.AI;

public class UnitMovement : NetworkBehaviour
{
    [SerializeField]
    private NavMeshAgent agent = null;
    [SerializeField]
    private Targeter targeter = null;
    [SerializeField] float chaseRange = 5f;

    #region Server

    public override void OnStartServer()
    {
        GameOverHandler.ServerOnGameOver += ServerHandleGameOver;
    }

    public override void OnStopServer()
    {
        GameOverHandler.ServerOnGameOver -= ServerHandleGameOver;
    }

    [Server]
    private void ServerHandleGameOver()
    {
        agent.ResetPath();
    }

    [ServerCallback]
    private void Update()
    {
        Targetable target = targeter.GetTarget();

        if (target != null)
        {
            // if outside chase range
            if ((target.transform.position - transform.position).sqrMagnitude > (chaseRange * chaseRange))
            {
                // chase
                agent.SetDestination(target.transform.position);
            }
            else if (agent.hasPath)
            {
                agent.ResetPath();
            }

            return;
        }

        if (!agent.hasPath) return;
        if (agent.remainingDistance > agent.stoppingDistance) return;

        agent.ResetPath();
    }

    [Command]
    public void CmdMove(Vector3 intendedPosition)
    {
        ServerMove(intendedPosition);
    }

    [Server]
    public void ServerMove(Vector3 intendedPosition)
    {
        targeter.ClearTarget();

        if (ValidatePosition(intendedPosition))
        {
            agent.SetDestination(intendedPosition);
        }
    }

    private bool ValidatePosition(Vector3 intendedPosition)
    {
        // free postion on navmesh
        return (NavMesh.SamplePosition(intendedPosition, out NavMeshHit hit, 1f, NavMesh.AllAreas));
    }

    #endregion

}
