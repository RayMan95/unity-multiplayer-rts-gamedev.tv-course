using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targeter : NetworkBehaviour
{
    [SerializeField] private Targetable target;

    public Targetable GetTarget()
    {
        return target;
    }

    public override void OnStartServer()
    {
        GameOverHandler.ServerOnGameOver += ServerHandleGameOver;
    }

    public override void OnStopServer()
    {
        GameOverHandler.ServerOnGameOver -= ServerHandleGameOver;
    }



    #region Server

    [Command]
    public void CmdSetTarget (GameObject targetGameObject)
    {
        if (!targetGameObject.TryGetComponent<Targetable>(out Targetable newTarget)) return;

        this.target = newTarget;
    }

    [Server]
    private void ServerHandleGameOver()
    {
        ClearTarget();
    }

    [Server]
    public void ClearTarget ()
    {
        target = null;
    }

    #endregion

    #region Client



    #endregion
}
