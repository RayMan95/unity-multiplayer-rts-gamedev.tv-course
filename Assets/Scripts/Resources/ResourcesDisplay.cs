using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourcesDisplay : MonoBehaviour
{
    [SerializeField] private TMP_Text resourcesText = null;

    private RTSPlayer player;

    private void Start()
    {
        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();

        player.ClientOnResourcesUpdated += ClientHandleResourcesUpdated;
    }

    private void Update()
    {
        ClientHandleResourcesUpdated(player.GetResources());
    }

    private void OnDestroy()
    {
        if (player != null)
        {
            player.ClientOnResourcesUpdated -= ClientHandleResourcesUpdated;
        }
    }

    private void ClientHandleResourcesUpdated(int resources)
    {
        resourcesText.text = $"Resources: {resources}";
    }
}
